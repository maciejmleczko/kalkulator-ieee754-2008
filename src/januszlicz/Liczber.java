

package januszlicz;

/**
 *
 * @author maciejmleczko
 */
public class Liczber{
    public String ieee754(Float n) {
        Integer k;
        if(n>0) {
            k = Float.floatToRawIntBits(-n);
        }
        else {
        k = Float.floatToRawIntBits(n);
        }
        String m = Integer.toBinaryString(k);
        String wykladnik;
        String mantysa;
        if(n<0) {
            wykladnik = "1" + '|' + m.substring(1, 9);
            mantysa = m.substring(9);
        }
        else {
            wykladnik = "0" + '|' + m.substring(1, 9);
            mantysa = m.substring(9);
        }
        String wynik = wykladnik+'|'+mantysa;
        return wynik;
    }
    public String dodawanie(Float a, Float b) {
        Float n = a+b;
        Integer k;
        if(n>0) {
            k = Float.floatToRawIntBits(-n);
        }
        else {
        k = Float.floatToRawIntBits(n);
        }
        String m = Integer.toBinaryString(k);
        String wykladnik;
        String mantysa;
        if(n<0) {
            wykladnik = "1" + '|' + m.substring(1, 9);
            mantysa = m.substring(9);

        }
        else {
            wykladnik = "0" + '|' + m.substring(1, 9);
            mantysa = m.substring(9);
        }
        
        String wynik = wykladnik+'|'+mantysa;
        return wynik;
    }
    public String odejmowanie(Float a, Float b) {
        Float n = a-b;
        Integer k;
        if(n>0) {
            k = Float.floatToRawIntBits(-n);
        }
        else {
        k = Float.floatToRawIntBits(n);
        }
        String m = Integer.toBinaryString(k);
        String wykladnik;
        String mantysa;
        if(n<0) {
            wykladnik = "1" + '|' + m.substring(1, 9);
            mantysa = m.substring(9);
        }
        else {
            wykladnik = "0" + '|' + m.substring(1, 9);
            mantysa = m.substring(9);
        }
        
        String wynik = wykladnik+'|'+mantysa;
        return wynik;
    }
    public String mnozenie(Float a, Float b) {
        Float n = a*b;
        Integer k;
        if(n>0) {
            k = Float.floatToRawIntBits(-n);
        }
        else {
        k = Float.floatToRawIntBits(n);
        }
        String m = Integer.toBinaryString(k);
        String wykladnik;
        String mantysa;
        if(n<0) {
            wykladnik = "1" + '|' + m.substring(1, 9);
            mantysa = m.substring(9);
        }
        else {
            wykladnik = "0" + '|' + m.substring(1, 9);
            mantysa = m.substring(9);
        }
        
        String wynik = wykladnik+'|'+mantysa;
        return wynik;
    }
    public String dzielenie(Float a, Float b) {
        Float n = a/b;
        Integer k;
        if(n>0) {
            k = Float.floatToRawIntBits(-n);
        }
        else {
        k = Float.floatToRawIntBits(n);
        }
        String m = Integer.toBinaryString(k);
        String wykladnik;
        String mantysa;
        if(n<0) {
            wykladnik = "1" + '|' + m.substring(1, 9);
            mantysa = m.substring(9);
        }
        else {
            wykladnik = "0" + '|' + m.substring(1, 9);
            mantysa = m.substring(9);
        }
        
        String wynik = wykladnik+'|'+mantysa;
        return wynik;
    }
    public String kwadrat(Float n) {
        n=(float)Math.pow(n, 2);
        Integer k;
        if(n>0) {
            k = Float.floatToRawIntBits(-n);
        }
        else {
        k = Float.floatToRawIntBits(n);
        }
        String m = Integer.toBinaryString(k);
        String wykladnik;
        String mantysa;
        if(n<0) {
            wykladnik = "1" + '|' + m.substring(1, 9);
            mantysa = m.substring(9);
        }
        else {
            wykladnik = "0" + '|' + m.substring(1, 9);
            mantysa = m.substring(9);
        }
        
        String wynik = wykladnik+'|'+mantysa;
        return wynik;
    }
    public String pierwiastek(Float n) {
        n = (float)Math.sqrt(n);
        Integer k;
        if(n>0) {
            k = Float.floatToRawIntBits(-n);
        }
        else {
        k = Float.floatToRawIntBits(n);
        }
        String m = Integer.toBinaryString(k);
        String wykladnik;
        String mantysa;
        if(n<0) {
            wykladnik = "1" + '|' + m.substring(1, 9);
            mantysa = m.substring(9);
        }
        else {
            wykladnik = "0" + '|' + m.substring(1, 9);
            mantysa = m.substring(9);
        }
        
        String wynik = wykladnik+'|'+mantysa;
        return wynik;
    }
    public String odwrotnosc(Float n) {
        n = (float)Math.pow(n, -1);
        Integer k;
        if(n>0) {
            k = Float.floatToRawIntBits(-n);
        }
        else {
        k = Float.floatToRawIntBits(n);
        }
        String m = Integer.toBinaryString(k);
        String wykladnik;
        String mantysa;
        if(n<0) {
            wykladnik = "1" + '|' + m.substring(1, 9);
            mantysa = m.substring(9);
        }
        else {
            wykladnik = "0" + '|' + m.substring(1, 9);
            mantysa = m.substring(9);
        }
        
        String wynik = wykladnik+'|'+mantysa;
        return wynik;
    }
}
